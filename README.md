# Geovisio Proxy

Simple Nginx proxy used to route query to geovisio's services.

Used by panoramax.ign.fr

## License

Copyright (c) 2023-2024 GeoVisio Team

Licensed under [MIT](./LICENSE)
